# Summer of Code
### Learning Python by REST API example

## Introduction
This course aims to rehearse and improve your knowledge of Python. While it is required to have at least
a little previous experience, the overall learning curve is intentionally kept not too steep.

The main topic is the creation of a REST API. The end goal, however, is not to create a production ready API but rather
to have used most of the core principles and methods common to programming and Python in particular.

## Workflow
This course is heavily dependent on Git and common workflows that it offers. Every new lesson will be distributed as
a new release, including proper versioning. 

All the work that is done until a lesson is published will be under a separate branch. Once this work is complete, a
pull request will be created and the branch will be merged into `main`. The development branch will be deleted 
on merging.

**Topics that will be covered:**
* Data types
* Basic logic
* General coding do's and don'ts
* Git & workflows
* What is a REST API / fastapi & async
* OOP
* Decorators
* Wrappers
* Typing
* Composition (vs. Inheritance)
* Exception handling & debugging
* Client generation & simulating traffic

**Topics that might be covered (depending on time):**
* Database integration (document based -> MongoDB)
* Dockerizing application

## Installation
The required packages for this repository can be downloaded by either using the provided pipfile or requirements.txt.

### Pipenv
When creating a new pipenv, check the box "Install packages from Pipfile". If you missed this opportunity, simply
use the terminal of your IDE and run `pipenv update`

### Virtualenv
Run the following code in your terminal: `pip install -r requirements.txt`

### Conda/ Anaconda/ etc.
You're on your own buddy ;)

### System interpreter
Don't. Just don't.

## Running Jupyter Notebook
### Pipenv
Use the following command: `pipenv run jupyter notebook`

### Virtualenv
Use the following command: `jupyter notebook`

### Conda/ Anaconda/ etc.
Use the following command: `jupyter notebook` (I guess)

## Running Fastapi
This command should be working for all environments: `uvicorn main:app --reload`
(if it doesn't, Google is your friend and overlord ;) )

The "reload" flag automatically restarts the server whenever the code changes. This way, your changes will always be
reflected immediately.

Once the application is started, you can navigate to:
* [User interface](http://localhost:8000/docs)
* [Documentation](http://localhost:8000/redoc)

## Resources
* [Python 101](https://python101.pythonlibrary.org/)
* [GIT Basics](https://git-scm.com/book/en/v2)
* [REST API](https://en.wikipedia.org/wiki/Representational_state_transfer)
* [Fastapi Documentation](https://fastapi.tiangolo.com/)
* [Python Async](https://realpython.com/python-async-features/)
* [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification)
* [MongoDB](https://www.mongodb.com/)
* [Docker Basics](https://www.docker.com/101-tutorial)
* [Docker-Compose](https://docs.docker.com/compose/gettingstarted/)
